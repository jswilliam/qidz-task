# frozen_string_literal: true

Rails.application.routes.draw do
  
  # Mobile Api routes
  namespace :api do
    namespace :v1 do
    end
  end

  # Back admin routes start
  namespace :admin do
    resources :users
    resources :locations
    resources :reviews
    resources :movies
    resources :directors
    resources :actors
    # Admin root
    root to: 'application#index'
  end

  # Front routes start
  devise_for :users, only: %i[session registration], path: 'session',
                     path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }

  # Application root
  root to: 'admin/application#index'
  # Front routes end
end
