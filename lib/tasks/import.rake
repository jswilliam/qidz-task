require 'csv'    
task :import => :environment do
  movies_file = File.read('lib/assets/movies.csv')
  reviews_file = File.read('lib/assets/reviews.csv')
  csv_1 = CSV.parse(movies_file, :headers => true)
  csv_2 = CSV.parse(reviews_file, :headers => true)
  csv_1.each do |row|
    hash = row.to_h
    actor = Actor.find_or_create_by(name: hash['Actor'])
    director = Director.find_or_create_by(name: hash['Director'])
    location = Location.find_or_create_by(state: hash['Filming location'], city: hash['Country'])
    movie = Movie.find_or_create_by(title: hash['Movie'], description: hash['Description'], year: hash['Year'], director: director, location: location)
    movie.actors << actor unless actor.in? movie.actors
    movie.save
  end

  csv_2.each do |row|
    hash = row.to_h
    movie = Director.find_or_create_by(name: hash['Movie'])
    user = User.find_or_create_by(name: hash['User'])
    user.email = Faker::Internet.email
    user.save(validate: false)
    Review.find_or_create_by(stars: hash['Stars'], content: hash['Review'])
  end
end