# Readme
- create `database.yml`, `secrets.yml` and `.env` from example files
- run `rails db:setup`
- run `bundle exec rake import`