class MovieSerializer < ActiveModel::Serializer
  attributes :id, :description, :year, :location, :country
end
