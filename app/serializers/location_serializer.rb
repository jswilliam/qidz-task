class LocationSerializer < ActiveModel::Serializer
  attributes :id, :city, :state
end
