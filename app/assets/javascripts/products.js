$(document).ready(function() {
  var maxField = 10; //Input fields increment limitation
  var addButton = $(".add_button"); //Add button selector
  var wrapper = $(".field_wrapper"); //Input field wrapper
  var fieldHTML =
    '<div><input type="color" name="colors_field[]" value="" /><a href="javascript:void(0);" class="remove_button">&nbsp;<i class="fa fa-minus-circle"></i></a></div>'; //New input field html
  var x = 1; //Initial field counter is 1

  //Once add button is clicked
  $(addButton).click(function() {
    //Check maximum number of input fields
    if (x < maxField) {
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); //Add field html
    }
  });

  //Once remove button is clicked
  $(wrapper).on("click", ".remove_button", function(e) {
    e.preventDefault();
    $(this)
      .parent("div")
      .remove(); //Remove field html
    x--; //Decrement field counter
  });

  function getColorsValue() {
    return $("[name='colors_field[]']")
      .map(function() {
        return this.value;
      })
      .toArray()
      .join(",");
  }

  $("form").submit(function() {
    if ($("#multicolor")[0].checked === true) {
      $("input[name='product[colors]'").val("All colors availabe");
    } else {
      $("input[name='product[colors]'").val(getColorsValue());
    }
  });
});
