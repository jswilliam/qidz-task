# frozen_string_literal: true

class ModelMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.model_mailer.new_record_notification.subject
  #
  default from: 'app@ebnosoft.com'
  def new_inquiry_notification(inquiry)
    @inquiry = inquiry
    mail to: ENV['MAILGUN_AUTHORIZED_EMAIL']
  end
end
