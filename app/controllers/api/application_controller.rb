# frozen_string_literal: true

module Api
  class ApplicationController < ActionController::Base
    before_action :authenticate
    respond_to :json
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response
    rescue_from ActionController::ParameterMissing, with: :render_missing_params

    private

    def authenticate
      api_key = request.headers['X-Api-Key']
      @user = User.where(api_key: api_key).first if api_key
      unless @user.present?
        render json: { auth: { message: 'Missing or invalid API Key.' } },
               status: 401
      end
    end

    def render_unprocessable_entity_response(exception)
      render json: exception.record.errors, status: :unprocessable_entity
    end

    def render_not_found_response(exception)
      render json: { error: exception.message }, status: :not_found
    end

    def render_missing_params(exception)
      render json: { error: exception.message }, status: :unprocessable_entity
    end
  end
end
