class Movie < ApplicationRecord
  belongs_to :director
  has_many :reviews
  has_and_belongs_to_many :actors
  belongs_to :location
end
