class AddLocationToMovie < ActiveRecord::Migration[5.2]
  def change
    add_column :movies, :location_id, :integer
    add_index  :movies, :location_id
  end
end
