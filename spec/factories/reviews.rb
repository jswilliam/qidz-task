FactoryGirl.define do
  factory :review do
    stars 1
    content "MyText"
  end
end
