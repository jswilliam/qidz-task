FactoryGirl.define do
  factory :movie do
    description "MyText"
    year 1
    location "MyString"
    country "MyString"
  end
end
