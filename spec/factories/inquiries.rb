FactoryGirl.define do
  factory :inquiry do
    message "MyText"
    phone "MyString"
    email "MyString"
  end
end
