FactoryGirl.define do
  factory :contact do
    name "MyString"
    phone "MyString"
    email "MyString"
    company "MyString"
    notes "MyText"
  end
end
