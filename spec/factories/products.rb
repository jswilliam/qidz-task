FactoryGirl.define do
  factory :product do
    name "MyString"
    size 1
    colors "MyString"
    min_quantity 1
    description "MyText"
    type ""
  end
end
